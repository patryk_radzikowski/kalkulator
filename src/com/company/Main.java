package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println("Podaj wyrażenie matematyczne:");
        Reader reader = new Reader();
        Operations operations = new Operations();

        String operation = reader.readFromConsole();

        String[] dividedOperation = operation.split("\\+");

        double result = operations.add(Double.parseDouble(dividedOperation[0]), Double.parseDouble(dividedOperation[1]));
        System.out.println(result);
    }
}